//notice: written in SciLab

//pt 1
	h = 6.62607015E-34		//the value of the planck's constant
	h/(2*%pi)		//result

//pt 2
	sind(30/%e)		//sind -> sinus function taking an argument in degrees; %e -> euler's number

//pt 3
	(hex2dec('00123d3'))/(2.455E23)		//a hexadecimal number converted into a decimal one and then divided by the number given in the instruction

//pt 4
	sqrt(%e - %pi)		//sqrt function finds the square root of a given argument; %pi -> pi = 3.14...

//pt 5
	new_pi = %pi * 1E10		//multiplication of pi by 10 to the 10th power to "move" the comma behind the digit i'm looking for
	pi_round = floor(new_pi)		//i'm rounding down the "new pi" i received from multiplication to ensure the tenth no-longer-decimal digit remains unchanged by the rounding process
	d = 10		//introduction of variable d, which will be used to divide the rounded "new pi" in the next step
	r = pi_round - fix(pi_round./d).*d 		//the remainder r gives the result; fix() function rounds towards zero

//pt 6
	no_days = datenum(2019, 10, 14) - datenum(1999, 06, 04)		//using datenum() function

//pt 7
	hex = hex2dec('aabb')		//conversion of a hexadecimal into decimal
	er = 6371		//earth's radius in kilometers
	atan((%e^((sqrt(7)/2) - log(er/1E5))/hex))		//result

//pt 8
	m = 6.02214076E23		//the value of one mole
	micro_m = m * 0.2E-6		//conversion into 0.2 micromoles
	a = 9		//number of atoms in ethanol
	no_atms = a * micro_m		//number of atoms in 0.2 micromoles of ethanol

//pt 9
	x = 1/101		//the ratio 1:100
	pm = x * 1000		//conversion into per milles